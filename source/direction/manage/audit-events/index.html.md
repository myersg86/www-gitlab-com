---
layout: markdown_page
title: "Category Direction - Audit Events"
description: "In order to manage, or prove, compliance you must have visibility into all of the activity within your GitLab environment. Learn more here!"
canonical_path: "/direction/manage/audit-events/"
---

- TOC
{:toc}

## Audit Events

| | | |
| --- | --- | --- |
| **Stage** | **Maturity** | **Content Last Reviewed** |
| [Manage](/direction/dev/#manage) | [Viable](/direction/maturity/) | `2020-09-21` |

## Introduction and how you can help

Thanks for visiting this direction page for Audit Events in GitLab. If you'd like to provide feedback or contribute to our vision, please open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/1985).

“Compliance” is a term that evokes images of a large and slow-moving organization, but it applies to every instance with more than one user. Compliance can help you track the activity of your users and systems and validate those activities against company policy and procedures. Universally, legal and regulatory compliance frameworks require reliable, secure, and performant audit logging capabilities. To manage and verify compliance you need visibility for all activity within your GitLab environment.

### Overview

GitLab is an application used by people. These users interact with shared resources like projects, comments, and groups. Administrators, especially in complex instances with high levels of activity and membership, want to ensure that all activity is adhering to their corporate rules. They need the ability to see and backtrace activity if an event requires further investigation. If we’re not offering a comprehensive set of logs, instances won’t be able to feel confident they’ll have the information necessary to answer critical questions during an audit or incident investigation. Activity in GitLab should be **fully transparent** to administrators.

The **Audit Events** category is focused on capturing 100% of user-driven events within GitLab, making that data easily available (via APIs, webhooks, and exportable reports), and providing a reasonable duration of storage for the data that follows compliance requirements (7 years).

#### Target Audience

* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)

#### Challenges to address

It's important that we’re presenting information in a way that makes answering questions simple. It’s frustrating to comb through logs. Separation of duties questions like “who opened and merged this MR?” aren’t readily available in a logging system that logs individual events. GitLab should promote **traceability**; when a change is logged, we should enable an administrator to see the full context around the change.

* What success looks like: an administrator should be able to explore a logged event and see related changes and users. Clicking on the recorded event for a comment in a merge request, for instance, should show more contextual data around the merge request itself (when it was opened, merged, and who interacted with it). This should also be possible to do via the API.

Audit events are a necessary aspect of compliance, but more important is leveraging audit events to inform proactive decisions and actions. Every customer differs in their policies and procedures, but also in their security and compliance culture and mindset. It will be critical to provide customers with smart, proactive alerts about anomalous activity in a way that's easy to setup and take action on. Flexibility is key because some customers will want to take programmatic action (e.g. automatically block a user's account) and some will want to take a "lighter touch" approach (e.g. notify an administrator, but take no further action).

* What success looks like: you should be able to define anomalous user behavior and GitLab's audit events should automatically flag activity matching that criteria. These definitions should be highly configurable and range from the simple to the complex.

GitLab moves quickly, releasing new features every month. With each release, the challenge is to capture all missing audit events, and then capture audit events associated with new features.

* What success looks like: GitLab captures all current audit events and has a framework to ensure all new features ship with auditable events captured where appropriate.

Organizations, especially enterprise, have complex services and tooling built to integrate with their existing data analytics processes. This means that GitLab needs to ensure data parity between the audit events table within the UI and the audit events API. It also means incorporating different types of data ingestion for organizations, such as webhooks for various components within GitLab that are critical for an organization's custom tooling and analytics.

* What success looks like: GitLab has complete feature parity between the application's audit events tables and the audit events API and a comprehensive webhook system.

Excellence in this category means that we’re solving for all of these needs: **transparency**, **traceability**, **configuration**, and **monitoring**.

### Where we are Headed

**Audit Events** are a core component of any compliance program. An organization needs to know what activity is occuring on their systems in order to proactively manage those systems. Traditionally, audit logs have been technical, verbose, and difficult to manage for technical people, let alone non-technical people. The **Audit Events** category wants to ensure that anyone, even non-technical people, can easily find all of the data they're looking for, in as little time as possible, and with as much granularity as possible.

The three key themes for this category are: granularity, simplicity, and navigability.

##### Granularity

An audit events system is incomplete if it cannot answer any questions about who did what, when, and on what system. GitLab has the breadth and depth of insight to be able to shed light on the complexities of the devops workflows your organization employs. Not only should GitLab provide 100% of user-driven events, but those events should be traceable to other entities or events in the system. We want to provide you with an experience that answers your auditing questions, but also delights you while you're answering them.

The capture of all necessary audit events leads to a more reliable auditing process for your team and organization. This will enhance your ability to deal with any incident or anomaly.

##### Simplicity

We want to make it simple to work with GitLab audit events. When you use GitLab to analyze your logs, we want you to feel the joy of solving problems, finding missing puzzle pieces, and supporting your organization in a material way. You shouldn't need to know how to write code or navigate complex user interfaces. You should be able to sign in and start finding answers.

By making the **Audit Events** experience super simple, we hope to alleviate the time commitment and headache our users typically experience when searching for data in this context. 

##### Time savings

Anything related to audit or compliance is usually a major time commitment. Time spent building custom tooling and reporting to searching through the data in these, or other systems, for specific answers is time away from value-adding activities. Additionally, the person doing the analysis frequently does not specialize in compliance. This introduces friction because these people are being asked to do a task they're not passionate about, isn't part of their job, and/or is frustrating for them to fulfill.

By making GitLab audit events simple and friendly, you can minimize the time commitment, alleviating these feelings of frustration and tedium so you can focus on your other important responsibilities.

### What’s Next & Why

**Audit Events** is the highest priority for Manage:Compliance because it is a fundamental component of any compliance program and our customers continue to emphasize this at every opportunity. This means the Compliance group will be working on [refactoring the AuditEvent model](https://gitlab.com/groups/gitlab-org/-/epics/2765) to improve the performance of **Audit Events** and simplify the process of adding additional audit events. We will also be developing a framework to enable other product groups at GitLab and community contributors to easily add audit events with new features or where gaps are identified.

Since this category is currently [Viable](https://about.gitlab.com/direction/maturity/), we’re pursuing completeness and **transparency** by adding additional activities to our audit logs. You can view the Audit Events **complete** maturity epic [here](https://gitlab.com/groups/gitlab-org/-/epics/1217). We'll be focused specifically on events such as:

* [Pipeline job activity](https://gitlab.com/gitlab-org/gitlab/-/issues/215910)
* [Changes to Protected Environments](https://gitlab.com/gitlab-org/gitlab/-/issues/216164)
* [Failed git push and fetch actions](https://gitlab.com/gitlab-org/gitlab/-/issues/11650)
* [SAML configuration changes](https://gitlab.com/gitlab-org/gitlab/-/issues/80710)
* [Secret variable changes](https://gitlab.com/gitlab-org/gitlab/-/issues/8070)
* [Project repo setting changes](https://gitlab.com/gitlab-org/gitlab/-/issues/8069)
* [Project CI/CD changes](https://gitlab.com/gitlab-org/gitlab/-/issues/8073)

This is not an exhaustive list, but demonstrates our focus on 100% of user-driven events across all levels: instance, group and project. Making **Audit Events** complete is critical for compliance-minded organizations because a weakness in audit logging is a weakness in compliance posture. We want to ensure that GitLab's audit events are comprehensive and helping to drive success for customers in a simple, friendly way.

We'll also be working on ensuring that audit logs can [scale sufficiently](https://gitlab.com/groups/gitlab-org/-/epics/641) without losing data and are [comprehensive](https://gitlab.com/groups/gitlab-org/-/epics/736).

* Audit logs necessarily need to be scalable and reliable. We cannot accept a scenario where audit logs cannot be used due to performance issues or have data integrity issues. This is why we'll be concurrently focusing on the performance of the audit events system while continuing to add events for comprehensive coverage.

### Maturity

Currently, GitLab’s maturity in Audit Events is **viable**. Here’s why: GitLab currently offers an audit log system, but it does not capture 100% of user-driven events. While we’re iterating and capturing more events over time, our users demand a comprehensive view of activity in application logs. However, the events being captured are useful at answering valuable questions on GitLab instances, and are [used to solve real problems](https://about.gitlab.com/direction/maturity/).

* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline for this category's evolution.

Advancing this category to a [**complete** state](https://gitlab.com/groups/gitlab-org/-/epics/1217) means having a robust, complete set of logs that captures the vast majority of user activity. These logs are structured and easily parsed; while GitLab may not actively guard against threats, an administrator can easily track down the history behind nearly any change. Additionally, these audit logs should be usable; an administrator should be able to ingest them into their SIEM tool of choice or search for events easily in the GitLab UI and via our API.

**Lovable** consists of compliance adding active monitoring for Audit Events to be truly trusted by users. The application is able to demonstrate security and compliance on an ongoing basis, in a single view, and actively monitors for (and defends against) suspicious behavior. An administrator or auditor can check a dashboard to be brought up to speed - or simply get some peace of mind.

### User Success Metrics

We'll know we're on the right track with **Audit Events** based on the following metrics:

* More audit event searches via the [application UI](https://docs.gitlab.com/ee/administration/audit_events.html) and [audit events API](https://docs.gitlab.com/ee/api/audit_events.html)
* More [audit events CSV exports](https://gitlab.com/gitlab-org/gitlab/issues/1449)
* Fewer GitLab issues created to add audit events

### Competitive landscape

* [GitHub Audit Logs](https://help.github.com/en/github/setting-up-and-managing-organizations-and-teams/reviewing-the-audit-log-for-your-organization)
* [Azure DevOps Audit](https://docs.microsoft.com/en-us/azure/devops/organizations/settings/azure-devops-auditing?view=azure-devops&tabs=preview-page)
* [Bitbucket Auditing](https://confluence.atlassian.com/bitbucketserver/auditing-in-bitbucket-776640417.html)

### Analyst landscape

The primary feedback we've received from analysts about this category has centered around programmatic alerting. Providing smart audit logs that can notify appropriately personnel of anomalous activity or specific, defined events can really elevate the value of **Audit Events** for our customers.

### Top Customer Success/Sales issue(s)

* [Comprehensive audit log](https://gitlab.com/groups/gitlab-org/-/epics/736). CS, sales, and users all want a set of logs that captures the vast majority of events in GitLab. Since you don't know what you need until you need it, customers expect us to log everything.

### Top user issue(s)

* [Comprehensive audit log](https://gitlab.com/groups/gitlab-org/-/epics/736).

### Top Vision issue(s)

* [Behavioral monitoring MVC](https://gitlab.com/groups/gitlab-org/-/epics/259). With the volume of events in GitLab, effective monitoring won't be possible without automated alerting.

