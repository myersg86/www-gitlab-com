---
layout: markdown_page
title: "FY21 Architecture Roadmap"
---

## FY21 Architecture Roadmap

1. [GitLab.com Migration to Cloud Native](https://about.gitlab.com/handbook/engineering/infrastructure/production/kubernetes/gitlab-com/)
   * [Cloud Native Build Logs](https://gitlab.com/groups/gitlab-org/-/epics/4402)
   * [Cloud Native GitLab Pages](https://gitlab.com/groups/gitlab-org/-/epics/4403)
