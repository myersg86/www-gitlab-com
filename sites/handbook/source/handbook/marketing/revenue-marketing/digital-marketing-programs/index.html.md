---
layout: handbook-page-toc
title: "Digital Marketing Programs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Digital Marketing Programs (DMP)

The Digital Marketing Programs team attracts new visitors through organic and paid channels, tests incremental changes for conversion rate improvement, builds and manages digital marketing campaigns, and tracks campaign performance.

* [Role](/job-families/marketing/digital-marketing-programs-manager/)
* [Responsibilities](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/)

## DMP responsibilities
* Paid media management

## Reporting

Information about reporting done by the Digital Marketing Programs team and across the Marketing functional group can be found in the [Business Operations - Reporting](/handbook/business-ops/resources/#reporting) section.

## Digital Marketing Tools

We use a variety of tools to support other teams within the company. Details about the tech stack, who has access and the system admins are found on the [Tech Stack Applications](/handbook/business-ops/tech-stack-applications/#tech-stack-applications) page of the Business Operations handbook section.



