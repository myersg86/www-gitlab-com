---
layout: markdown_page
title: "Why GitLab Over GitHub for the Business Decision Maker"
canonical_path: "/devops-tools/github/Why-GitLab-Over-GitHub-for-the-Business-Decision-Maker.html"
description: "Why choose GitLab Over GitHub for the Business Decision Maker."
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}


## GitHub Strenghts

**User Base**
- Large user base of developers using GitHub.

**Open Source Support**
- Many Open Source projects hosted on GitHub, although GitHub itself is not Open Source.

## GitHub Limitation and Challenges

**End to End DevOps**
- GitHub requires multiple tools stitched together to provide an end to end DevOps solution.  GitLab provides a single platform for DevOps, with tools for different stages pre-integrated and working seamlessly.

**Scaling**
- Feedback from GitHub customers is that GitHub Enterprise (GHE) has trouble scaling. essentially is in a single box.  The only way to scale the system is by adding more CPU and memory.  This taps out at some scale.  Data suggests GitHub is unable to scale beyond 2000 users in many instances.  After maximizing CPU and memory in a single box, the only way to scale is to add separate boxes or decentralize.  This leads to code fragmentation.
[GitLab is enterprise class](/solutions/enterprise-class/) and scales to > 32K users.

**High Availability**
- GitHub HA solution is at best a warm-standby.  This approach could lead to more downtime and even information loss if backups are not made frequently.

**Zero Downtime Upgrades**
- GitHub does not provide Zero Downtime Upgrades, with downtime for upgrades lasting as many as four hours.  This is a major requirement for most enterprises.  GitLab provides Zero Downtime upgrades.

**Product Enhancement**
- As a large repository of Open Source projects, GitHub itself is not Open Source.  Making customers dependent solely on GitHub for product improvements.

**Azure DevOps vs. GitHub confusion**
- There is market confusion over the future of Azure DevOps vs GitHub.  Customers have questions:
    - Will Azure DevOps legacy features be ported into GitHub?
    - What will the new leadership team look like?
    - Will there be any adjustments in product pricing?
    - What will the new support model look like?
    - Will Azure DevOps be completely replaced by GitHub?

## GitHub's Selling Strategy

**Product Updates**
- Over the past year, most of GitHub's product improvements were for their Self-hosted runners and Actions APIs.

**Tactics GitHub uses to Differentiate Themselves**

| This is How GitHub Responds to GitLab   |                                                                                                                                                                 |
|-----------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| **Leverage Their Community**            | Projects shared by community<br>GitHub Apps<br>GHA Marketplace<br>                                                                                        |
| **Working to neutralize our advantage** | Ongoing GHA Improvements<br>GHA API with community is creating workarounds in areas they are deficient<br>Security offering leverages their community<br> |
| **Modernize DevOps**                    | Mobile App for GitHub                                                                                                                                         |
| **Sell-with Azure and Other MS Assets** | GitHub leverages Microsoft by making it easier to buy GitHub in conjunction with other Microsoft products such as Azure Cloud.                                |

## 5 Key GitLab Differentiators

**1. Broader Breath of Offering**
- GitLab’s DevOps capabilities are deep and broad.  Customers need to rely on several moving parts with GitHub, whereas with GitLab its a single application.

![GitLab GitHub Comparison Chart](/devops-tools/github/images/github_comparison.png)


**2. Better Capabilities**
- In areas of overlap, GitLab has superior capabilities.  Some of the benefits are shorter cycle times, better security, increased collaboration amongst developers.

| GitLab Capabilities That Are Missing In GitHub                       |                                                                                                                                                           |
|------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Issue Management       | Track complete development lifecycle from plan to deployment, Maintain Issues across multiple projects.                                                                                                 |
| Source Code Management | Code review tailored for large teams, Model and manage large, complex development teams, Broader support for Repo types                                                                                 |
| CI-CD                  | AutoDevOps, Built-in CI-CD without need for plug ins, better support for Cloud Native Development, Faster Build and Deploy Times.  GitHub CI-CD increases build times, and increases maintenance costs. |
| DevSecOps              | Single pane of glass for all security vulnerabilities, DAST, Deployment and container scanning                                                                                                          |
| Secure DevOps Process  | Maintain confidentiality of issues and activity, fine grained controls on submit, code, build and deploy, securely access remote assets                                                                 |
| Compliance             | Out of the box reporting support for Financial Services Regulatory Complaince, PCI, HIPPA and other regulations. 

**3. Better Customer Support**
- GitLab provides a support model that is simple and easy to understand.  Unlinke GitHub, GitLab has a single contact portal for support questions managed by one cohesive team.  GitHub's support model is confusing, with two support portals and two isolated support paths that customers must choose from.  Visit the [GitLab vs. GitHub Support](https://about.gitlab.com/devops-tools/github/GitLab-vs-GitHub-Support.html) page for a detailed comparison between GitLab's and GitHub's support offerings.  

**4. Superior Value**
- Both GitLab and GitHub offer Free License Tiers and 3 Paid License Tiers.  Though there are some similarities between GitLab's and GitHub's License Tiers, there are also some GitLab differences that yields greater value delivered by GitLab within each Tier.  Visit the [GitLab vs. GitHub License](https://about.gitlab.com/devops-tools/github/GitLab-vs-GitHub-License.html) page for a detailed comparison between GitLab's and GitHub's Licensing Tiers.


**5. Scaling, HA and Zero-Downtime Upgrade Capabilities**
- Scaling
    - [GitLab is enterprise class](/solutions/enterprise-class/) and scales to > 32K users. Here's a link to the architecture when deployed in [Amazon](https://docs.gitlab.com/ee/install/aws/).  This feature when used with Amazon's AutoScaling feature ensures that you can have nodes turned on and off as needed based on demand.  This leads to significant cost savings, while maintaining the developer experience from a performance standpoint. GitLab runners are also mutable, one can spin up runners as needed and only pay for what one needs to use.

- High-Availability
    - GitLab provides HA from two standpoints.  First the Application itself is Highly Available through seamless failovers into remaining nodes.  Second, the data is highly available through the use of [Gitaly](https://docs.gitlab.com/ee/administration/gitaly/index.html).  Gitaly is a service that provides high-level RPC access to Git repositories. GitLab fails over seamlessly - if a Gitaly node or another node drops there is no downtime.  In case of a catastrophic event, GitLab can fail over to another Geo.  This process in case of catastropic failure takes only 5-20 minutes.  Business is up and running at full speed without problems.

- Zero-Downtime Upgrades
    - Due to the strong HA architecture GitLab is able to provide zero downtime upgrades.

## GitLab vs GitHub Innovation Pace

![GitLab GitHub Innovation Chart](/devops-tools/github/images/Gitlab-vs-GitHub-Innovation.png)

